class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  has_one_attached :picture

  validates_presence_of     :name
  validates_presence_of     :address
  validates :phone,:presence => true, :numericality => true, :length => { :minimum => 10, :maximum => 15 }
end
