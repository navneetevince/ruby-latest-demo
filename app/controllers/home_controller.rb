class HomeController < ApplicationController
	
	def index
		if current_user && current_user.isadmin?
			@user=User.where(isadmin: false)
		end	
	end
end
